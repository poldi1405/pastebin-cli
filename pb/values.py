# API key if not specified otherwise
APIKEY = ""

# values for creating new pastes
URL_NEWPASTE = "https://pastebin.com/api/api_post.php"
REQ_NEWPASTE = ["paste_code"]
ERR_NEWPASTE = {
	"Bad API request, invalid api_option": "",
	"Bad API request, invalid api_dev_key": "",
	"Bad API request, IP blocked": "",
	"Bad API request, maximum number of 25 unlisted pastes for your free account": "",
	"Bad API request, maximum number of 10 private pastes for your free account": "",
	"Bad API request, api_paste_code was empty": "",
	"Bad API request, maximum paste file size exceeded": "",
	"Bad API request, invalid api_expire_date": "",
	"Bad API request, invalid api_paste_private": "",
	"Bad API request, invalid api_paste_format": "",
	"Bad API request, invalid api_user_key": "",
	"Bad API request, invalid or expired api_user_key": "",
}

# values for logging in users
URL_LOGIN = "https://pastebin.com/api/api_login.php"
REQ_LOGIN = ["user_name", "user_password"]
ERR_LOGIN = {
	
}

# values for listing a users pastes
URL_LISTPASTES = "https://pastebin.com/api/api_post.php"
REQ_LISTPASTES = ["user_key", "results_limit"]

# values for deleting pastes
URL_DELETEPASTE = "https://pastebin.com/api/api_post.php"
REQ_DELETEPASTE = ["user_key", "paste_key"]

# values for retrieving a users settings
URL_GETSETTINGS = "https://pastebin.com/api/api_post.php"
REQ_GETSETTINGS = ["user_key"]

# values for retrieving private pastes of a user
URL_GETUSERPASTE = "https://pastebin.com/api/api_raw.php"
REQ_GETUSERPASTE = ["user_key", "paste_key"]

# values for getting pasted text
URL_GETPASTE = "https://pastebin.com/raw/"
